package endpoints

import (
	"capstone-dot3/api/types"
	"fmt"
	"io/ioutil"
	"net/http"
	"net/url"
	"os"

	"github.com/labstack/echo"
)

func GetTranscripts(c echo.Context) error {
	response, err := http.Get("http://localhost:9091/api/queries/queryall")
	if err != nil {
		fmt.Printf("%s", err)
		os.Exit(1)
	} else {
		defer response.Body.Close()
		data, err := ioutil.ReadAll(response.Body)
		if err != nil {
			fmt.Printf("%s", err)
			os.Exit(1)
		}
		//fmt.Printf("%s\n", string(data))
		return c.String(http.StatusOK, string(data))
	}

	return c.JSON(http.StatusOK, "nope")
}

func GetStudents(c echo.Context) error {
	response, err := http.Get("http://localhost:3000/api/org.acme.acalink.Student")
	if err != nil {
		fmt.Printf("%s", err)
		os.Exit(1)
	} else {
		defer response.Body.Close()
		data, err := ioutil.ReadAll(response.Body)
		if err != nil {
			fmt.Printf("%s", err)
			os.Exit(1)
		}
		//fmt.Printf("%s\n", string(data))
		return c.String(http.StatusOK, string(data))
	}

	return c.JSON(http.StatusOK, "nope")
}

func GetInstitutions(c echo.Context) error {
	response, err := http.Get("http://localhost:3000/api/org.acme.acalink.Institution")
	if err != nil {
		fmt.Printf("%s", err)
		os.Exit(1)
	} else {
		defer response.Body.Close()
		data, err := ioutil.ReadAll(response.Body)
		if err != nil {
			fmt.Printf("%s", err)
			os.Exit(1)
		}
		//fmt.Printf("%s\n", string(data))
		return c.String(http.StatusOK, string(data))
	}

	return c.JSON(http.StatusOK, "nope")
}

func GetCourses(c echo.Context) error {
	response, err := http.Get("http://localhost:3000/api/org.acme.acalink.Course")
	if err != nil {
		fmt.Printf("%s", err)
		os.Exit(1)
	} else {
		defer response.Body.Close()
		data, err := ioutil.ReadAll(response.Body)
		if err != nil {
			fmt.Printf("%s", err)
			os.Exit(1)
		}
		//fmt.Printf("%s\n", string(data))
		return c.String(http.StatusOK, string(data))
	}

	return c.JSON(http.StatusOK, "nope")
}

func GetStudent(c echo.Context) error {
	id := c.Param("id")
	url := fmt.Sprintf("http://localhost:3000/api/org.acme.acalink.Student/%s", id)
	response, err := http.Get(url)
	if err != nil {
		fmt.Printf("%s", err)
		os.Exit(1)
	} else {
		defer response.Body.Close()
		data, err := ioutil.ReadAll(response.Body)
		if err != nil {
			fmt.Printf("%s", err)
			os.Exit(1)
		}
		//fmt.Printf("%s\n", string(data))
		return c.String(http.StatusOK, string(data))
	}

	return c.JSON(http.StatusOK, "nope")
}

func GetInstitution(c echo.Context) error {
	id := c.Param("id")
	url := fmt.Sprintf("http://localhost:3000/api/org.acme.acalink.Institution/%s", id)
	response, err := http.Get(url)
	if err != nil {
		fmt.Printf("%s", err)
		os.Exit(1)
	} else {
		defer response.Body.Close()
		data, err := ioutil.ReadAll(response.Body)
		if err != nil {
			fmt.Printf("%s", err)
			os.Exit(1)
		}
		//fmt.Printf("%s\n", string(data))
		return c.String(http.StatusOK, string(data))
	}

	return c.JSON(http.StatusOK, "nope")
}

func GetCourse(c echo.Context) error {
	id := c.Param("id")
	url := fmt.Sprintf("http://localhost:3000/api/org.acme.acalink.Course/%s", id)
	response, err := http.Get(url)
	if err != nil {
		fmt.Printf("%s", err)
		os.Exit(1)
	} else {
		defer response.Body.Close()
		data, err := ioutil.ReadAll(response.Body)
		if err != nil {
			fmt.Printf("%s", err)
			os.Exit(1)
		}
		//fmt.Printf("%s\n", string(data))
		return c.String(http.StatusOK, string(data))
	}

	return c.JSON(http.StatusOK, "nope")
}

func AddCourse(c echo.Context) error {

	u := "http://localhost:3000/api/org.acme.acalink.Course"

	data := types.Course{
		c.FormValue("id"),
		c.FormValue("name"),
		c.FormValue("desc"),
		c.FormValue("credits"),
		c.FormValue("institution"),
	}
	fmt.Printf("%s, %s, %s", data.ID, data.Name, data.Description)
	resp, err := http.PostForm(u,
		url.Values{"$class": {"org.acme.acalink.Course"}, "id": {data.ID}, "name": {data.Name},
			"desc": {data.Description}, "credits": {data.Credits}, "institution": {data.Institution}})
	if nil != err {
		return c.JSON(http.StatusOK, "nope")
	}

	defer resp.Body.Close()
	body, err := ioutil.ReadAll(resp.Body)

	if nil != err {
		return c.JSON(http.StatusOK, "nope")
	}

	fmt.Println(string(body[:]))
	return c.JSON(http.StatusOK, "Status:OK")
}

func AddInstitution(c echo.Context) error {

	u := "http://localhost:3000/api/org.acme.acalink.Institution"

	data := types.Institution{
		c.FormValue("id"),
		c.FormValue("name"),
		c.FormValue("primaryType"),
		c.FormValue("secondaryType"),
	}
	fmt.Printf("%s, %s, %s", data.ID, data.Name, data.PrimaryType)
	resp, err := http.PostForm(u,
		url.Values{"$class": {"org.acme.acalink.Institution"}, "id": {data.ID}, "name": {data.Name},
			"primaryType": {data.PrimaryType}, "secondaryType": {data.SecondaryType}})
	if nil != err {
		return c.JSON(http.StatusOK, "nope")
	}

	defer resp.Body.Close()
	body, err := ioutil.ReadAll(resp.Body)

	if nil != err {
		return c.JSON(http.StatusOK, "nope")
	}

	fmt.Println(string(body[:]))
	return c.JSON(http.StatusOK, "Status:OK")
}

func AddStudent(c echo.Context) error {

	u := "http://localhost:3000/api/org.acme.acalink.Student"

	data := types.Student{
		c.FormValue("id"),
		c.FormValue("firstName"),
		c.FormValue("middleName"),
		c.FormValue("lastName"),
	}
	fmt.Printf("%s, %s, %s", data.ID, data.FirstName, data.LastName)
	resp, err := http.PostForm(u,
		url.Values{"$class": {"org.acme.acalink.Student"}, "id": {data.ID}, "firstName": {data.FirstName},
			"middleName": {data.MiddleName}, "lastName": {data.LastName}})
	if nil != err {
		return c.JSON(http.StatusOK, "nope")
	}

	defer resp.Body.Close()
	body, err := ioutil.ReadAll(resp.Body)

	if nil != err {
		return c.JSON(http.StatusOK, "nope")
	}

	fmt.Println(string(body[:]))
	return c.JSON(http.StatusOK, "Status:OK")
}
