package main

import (
	"capstone-dot3/api/endpoints"

	"github.com/labstack/echo"
	"github.com/labstack/echo/middleware"
)

func main() {
	e := echo.New()
	e.Use(middleware.CORSWithConfig(middleware.CORSConfig{
		AllowOrigins: []string{"*"},
		AllowHeaders: []string{echo.HeaderOrigin, echo.HeaderContentType, echo.HeaderAccept},
	}))
	e.GET("/transcripts", endpoints.GetTranscripts)
	e.GET("/students", endpoints.GetStudents)
	e.GET("/institutions", endpoints.GetInstitutions)
	e.GET("/courses", endpoints.GetCourses)

	e.GET("/student/:id", endpoints.GetStudent)
	e.GET("/institution/:id", endpoints.GetInstitution)
	e.GET("/course/:id", endpoints.GetCourse)

	e.POST("/addstudent", endpoints.AddStudent)
	e.POST("/addinstitution", endpoints.AddInstitution)
	e.POST("/addcourse", endpoints.AddCourse)
	//e.POST("/addtranscript", endpoints.AddTranscript)

	e.Logger.Fatal(e.Start(":8081"))
}
