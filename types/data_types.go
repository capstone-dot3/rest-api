package types

type Course struct {
	ID          string `json:"id"`
	Name        string `json:"name"`
	Description string `json:"description"`
	Credits     string `json:"credits"`
	Institution string `json:"institution"`
}

type Courses struct {
	Courses []Course `json:"course"`
}

type Student struct {
	ID         string `json:"id"`
	FirstName  string `json:"firstname"`
	MiddleName string `json:"middlename"`
	LastName   string `json:"lastname"`
}

type Students struct {
	Students []Student `json:"student"`
}

type Institution struct {
	ID            string `json:"id"`
	Name          string `json:"name"`
	PrimaryType   string `json:"primarytype"`
	SecondaryType string `json:"secondarytype"`
}
